// Proxy_Math.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
using namespace std;
/**
 * "Subject"
 */

class Person
{
public:
    string Set_name(Person &pointer, int NumberOfEmployee);
    string Sname(int NumberOfEmployee)
    {
        return list[NumberOfEmployee];

    }
    int Snumber()
    {
        return number;
    }
private:
    string list[1000];
    int number;


};
string Person::Set_name(Person &pointer,int NumberOfEmployee)
{
    cout<<"Enter name"<<endl;
    cin>>list[NumberOfEmployee];
    pointer.number=NumberOfEmployee;
    return list[NumberOfEmployee];
}

class IMath //������������ �����
{
public:
    virtual double Add(int, double, double) = 0;
    virtual double Sub(int, double, double) = 0;
    virtual double Mul(int, double, double) = 0;
    virtual double Div(int, double, double) = 0;
	virtual int Get_index(Person &pointer, string)=0;
};
 
/**
 * "Real Subject"
 */
class Math : public IMath //�������� ������
{
public:

    virtual double Add(int index, double x, double y) 
    {

        if (index!=0)
        {
		return x + y;
        }
		else
        {
		return -1;
        }
    }
 
    virtual double Sub(int index, double x, double y) 
    {
        if (index!=0)
        {
		return x - y;
        }
		else
        {
		return -1;
        }
    }
 
    virtual double Mul(int index, double x, double y) 
    {
        if (index!=0)
        {
		return x * y;
        }
		else
        {
		return -1;
        }
    }
 
    virtual double Div(int index, double x, double y) 
    {
        if (index!=0)
        {
		return x / y;
        }
		else
        {
		cout<<"Operation not permited"<<endl;
		return -1;
        }
    }
	virtual int Get_index(Person &pointer,string name)
	{
		return 0;
	}
};
 
/**
 * "Proxy Object"
 */
class MathProxy : public IMath //������
{
public:
    MathProxy()
    {
        math = new Math();
    }
    virtual ~MathProxy()
    {
        delete math;
    }
	int Get_index(Person &pointer, string name)
	{
		for (int i=0; i<=pointer.Snumber(); i++)
            if(pointer.Sname(i)==name)
            {
                return 1;
            }
		return 0;
	}
    virtual double Add(int index,double x, double y) 
    {
        return math->Add(index,x, y);
		
    }
 
    virtual double Sub(int index,double x, double y) 
    {
        return math->Sub(index, x, y);
   }
 
    virtual double Mul(int index, double x, double y) 
    {
        return math->Mul(index, x, y);
    }
 
    virtual double Div(int index,double x, double y) 
    {
        return math->Div(index, x, y);
		
    }
 
private:
    IMath *math;//��������� �� �������� ������
};
  
int main() 
{
    // �������� ������ �������
    IMath *proxy = new MathProxy();
	Person workers;
    int count;
    cout<<"Vvedite kolichestvo sluzhash'ih"<<endl;
    cin>>count;
    for(int i=0;i<count;i++)
    {
    workers.Set_name(workers,i);
    }
	int index1=0;
    string name;
    bool flag;
    flag=false;
    char answer;
    const char Yes='Y';
    const char No='N';
    double A,B;
    while (flag==false)
    {
        cout<<"Would you like to perform a mathematical operation? Y/N"<<endl;
        cin>>answer;
        if (answer==No)
        {
            flag=true;
        }
                    
        if (answer==Yes)
        {
            cout<<"Enter your name"<<endl;
            cin>>name;
            cout<<"Enter A and B"<<endl;
            cin>>A;
            cin>>B;
            index1=proxy->Get_index(workers, name);
	
	        cout << name<<" "<< A<<" + "<<B<<" = " << proxy->Add(index1,4, 2) << endl;
	    	cout << name<<" "<< A<<" - "<<B<<" = " << proxy->Sub(index1,4, 2) << endl;
            cout << name<<" "<< A<<" * "<<B<<" = " << proxy->Mul(index1,4, 2) << endl;
            cout << name<<" "<< A<<" / "<<B<<" = " << proxy->Div(index1,4, 2) << endl;
        }
    }
    
    delete proxy;
    return 0;
}